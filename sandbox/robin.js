const name = 'Robin'

const user = {
  name,
}

const key = 'name'
const u = {
  firstname: 'firsttest',
  lastname: 'lasttes',
}

const userService = {
  getUserName(user) {
    return `${user.firstname} - ${user.lastname}`
  }
}

console.log(user)
console.log(userService.getUserName(u))
